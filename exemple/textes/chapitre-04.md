# Suites possibles

\lipsum[19-24]


\begin{sidewaysfigure}
    \centering
    \includegraphics[width=\linewidth]{images/image-a.jpg}
    \caption{Exemple d'insertion d'une figure en mode paysage}
    \label{fig:figure1}
\end{sidewaysfigure}


\lipsum[25-30]
