# Analyse des résultats

Exemple d'une référence à un chapitre. Ici nous faisons une référence au chapitre **\nameref{conduite-de-lenquuxeate}** de la page \pageref{conduite-de-lenquuxeate}.

Voir la documentation de *Pandoc* <https://pandoc.org/MANUAL.html#headers-and-sections> pour déterminer l'identifiant à utiliser. Si le titre du chapitre ou de la section contient des caractères accentués, alors les remplacer par les valeurs de la table suivante:

| **Caractère** | **Valeur** |
| :-------- | :------------------ |
| à | uxe0 |
| â | uxe2 |
| æ | uxe6 |
| ç | uxe7 |
| è | uxe8 |
| é | uxe9 |
| ê | uxea |
| ë | uxeb |
| î | uxee |
| ï | uxef |
| ô | uxf4 |
| œ | ux9c |
| ù | uxf9 |
| û | uxfb |
| ü | uxfc |



\lipsum[13-18]
