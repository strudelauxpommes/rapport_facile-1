# Titre de l'annexe deux

\lipsum[37-42]

| **Date** | **Contenu** |
| :-------- | :------------------ |
| 2017-01-12 | Introduction |
| 2017-01-19 | Définition du problème ou de l'opportunité |
| 2017-01-26 | Étude de faisabilité -- partie 1 |
| 2017-02-02 | Étude de faisabilité -- partie 2 |
| 2017-02-09 | Diagramme de contexte |
| 2017-02-16 | Modèle conceptuel |
| 2017-02-23 | **Examen intra** |
| 2017-03-02 | **Semaine de relâche** |
| 2017-03-09 | Cas d'utilisation -- partie 1 |
| 2017-03-16 | Cas d'utilisation -- partie 2 |
| 2017-03-23 | Événements système, diagramme de séquence et contrats |
| 2017-03-30 | Spécifications autres que fonctionnelles -- partie 1 |
| 2017-04-06 | Spécifications autres que fonctionnelles -- partie 2 |
| 2017-04-13 | Rapport, revue technique et communication |
| 2017-04-20 | Conclusion |
| 2017-04-27 | **Examen final** |

Table: Exemple d'un tableau

## Exemple d'inclusion d'une section d'un document pdf

\includepdf[pages=1-3,scale=0.70,pagecommand={},frame=true]{images/Rapport_Chaos_2015.pdf}
