# Titre de l'annexe un

\lipsum[31]

## Première section

\lipsum[32]

![Exemple d'insertion d'une figure](images/image-a.jpg)

### Première sous-section

\lipsum[33-34]

![Autre exemple d'insertion d'une figure](images/image-b.png)

### Deuxième sous-section

\lipsum[35-37]

## Deuxième section

\lipsum[38]

### Exemple de listing

~~~ {#code_1 .python numbers=left caption="Exemple d'un listing"}
def traiterInclusion(extrant, commande):
    sections = commande[:-1].split(':')
    nom_fichier = sections[1]
    ligne_debut = obtenirValeur(sections, 2, 1)
    ligne_fin = obtenirValeur(sections, 3, 999999)
    print(f'Inclusion de {nom_fichier} [{ligne_debut},{ligne_fin}]')
    with open(nom_fichier, 'r') as fichier:
        for i, ligne in enumerate(fichier.readlines(), 1):
            if ligne_debut <= i <= ligne_fin:
                extrant.write(ligne)
~~~
