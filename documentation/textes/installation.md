
# Installation

Bravo! Je suis fier de vous. Vous désirez poursuivre l'aventure, je vous en félicite. Le présent chapitre décrit l'installation des logiciels requis. Cette étape n'est à accomplir qu'une seule fois. Lorsqu'elle est complétée, vous aurez au bout des doigts un environnement de production de texte facile à utiliser, professionnel et faisant l'envie de tous.

\encadrer{Note}{}{
Utilisant Mac OS X depuis plus de dix ans, le contenu du présent chapitre se décline avec ce système d'exploitation. Tous les logiciels spécifiés existent également pour les systèmes Windows et Linux/Unix. Les utilisateurs de ces deux systèmes sont invités à adapter le contenu du chapitre en conséquence.
}

Voici deux liens proposés par Alexandre Paquet Fays pour faciliter l'installation de \LaTeX{} sous Linux:

- `<http://tug.org/texlive/doc/texlive-en/texlive-en.html#x1-140003>`
- `<http://tug.org/texlive/quickinstall.html>`

## Logiciels requis

Tous les logiciels requis sont disponibles sous licence libre. En voici la liste:

- une distribution complète de \LaTeX;

- Pandoc;

- Python 3;

- un éditeur de texte de votre choix;

- optionnellement, une application de gestion de version.

Rassurez-vous, vous n'avez à connaitre ni \LaTeX, ni Pandoc et ni Python 3. Ces applications sont utilisées en arrière-plan de façon transparente pour vous. Seuls, l'éditeur de texte et, optionnellement, le gestionnaire de version seront utilisés par vous.

### \LaTeX

Sous Mac OS X, la distribution complète de \LaTeX\ est _MacTeX_. [^instal-mactext] Le téléchargement de près de trois giga-octets peut être long avec un lien Internet lent. L'installation se fait facilement. Elle comprend \Hologo{XeLaTeX}.

### Pandoc

Le site de Pandoc [^instal-pandoc] contient un lien vers les instructions d'installation pour toutes les plateformes.

### Python 3

Mac OS X préinstalle la version 2 de Python. Nous avons besoin de la version 3. Les deux versions peuvent cohabiter sans problèmes. Le site officiel de Python [^instal-python] vous mène rapidement au téléchargement pour toutes les plateformes. Au moment d'écrire ces lignes, la version courante pour Python 3 était 3.9.0.

### Éditeur de texte

Il existe une multitude d'éditeurs de texte pour toutes les plateformes. Faites votre choix. Pour l'instant, j'utilise Atom [^instal-atom] avec plaisir. Atom offre plusieurs _packages_, dont un permettant la coloration syntaxique pour Markdown.

### Gestionnaire de version

L'utilisation d'un gestionnaire de version n'est pas obligatoire, mais drôlement pratique. Pour ma part, j'utilise _Git_ [^instal-git] avec un dépôt externe gratuit sur _Bitbucket_ [^instal-bitbucket] et le client _SourceTree_ [^instal-sourcetree], ces deux derniers sont offerts par la firme _Atlassian_ [^instal-atlassian].

## Vérification de l'installation

Pour vérifier que tout est bien installé, ouvrez l'application _Terminal_.

Pour \Hologo{XeLaTeX}, entrez la commande suivante [^instal-invite] : `$ xelatex --version`

Le résultat obtenu au moment d'écrire ces lignes était:

    XeTeX 3.14159265-2.6-0.999992 (TeX Live 2020)
    kpathsea version 6.3.2
    Copyright 2020 SIL International, Jonathan Kew and Khaled Hosny.
    There is NO warranty.  Redistribution of this software is
    covered by the terms of both the XeTeX copyright and
    the Lesser GNU General Public License.
    For more information about these matters, see the file
    named COPYING and the XeTeX source.
    Primary author of XeTeX: Jonathan Kew.
    Compiled with ICU version 65.1; using 65.1
    Compiled with zlib version 1.2.11; using 1.2.11
    Compiled with FreeType2 version 2.10.1; using 2.10.1
    Compiled with Graphite2 version 1.3.13; using 1.3.13
    Compiled with HarfBuzz version 2.6.4; using 2.6.4
    Compiled with libpng version 1.6.37; using 1.6.37
    Compiled with poppler version 0.68.0
    Using Mac OS X Core Text and Cocoa frameworks

Pour Pandoc, entrez la commande suivante: `$ pandoc --version`

Le résultat obtenu au moment d'écrire ces lignes était:

    pandoc 2.11.1.1
    Compiled with pandoc-types 1.22, texmath 0.12.0.3, skylighting 0.10.0.3,
    citeproc 0.1.1.1, ipynb 0.1
    User data directory: /Users/louis/.local/share/pandoc or /Users/louis/.pandoc
    Copyright (C) 2006-2020 John MacFarlane. Web:  https://pandoc.org
    This is free software; see the source for copying conditions. There is no
    warranty, not even for merchantability or fitness for a particular purpose.

Pour Python 3, entrez la commande suivante: `$ python3 --version`

Le résultat obtenu au moment d'écrire ces lignes était:

    Python 3.9.0

Tout semble bien installé. Passons maintenant à la dernière étape, celle de l'utilisation.


<!-- Notes de bas de page -->

[^instal-mactext]: <https://www.tug.org/mactex/>.
[^instal-pandoc]: <https://pandoc.org/>.
[^instal-python]: <https://www.python.org/>.
[^instal-atom]: <https://atom.io/>.
[^instal-git]: <https://git-scm.com/>.
[^instal-bitbucket]: <https://bitbucket.org/>.
[^instal-sourcetree]: <https://www.sourcetreeapp.com/>.
[^instal-atlassian]: <https://fr.atlassian.com/>.
[^instal-invite]: Le caractère `$` correspond à l'invite de commande. L'invite de commande varie d'un système à un autre.
