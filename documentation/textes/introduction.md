
# Introduction

La tâche de rédiger un rapport ou un mémoire est déjà assez compliquée au niveau du contenu sans que nous soyons obligés de nous battre avec un traitement de texte pour tenter de respecter la mise en page requise.

Ayant entendu des commentaires de plusieurs étudiants étant passés à travers cette épreuve, j'ai voulu voir si une solution plus facile était possible.

## Version 2 du projet

Le présent document décrit la version 2 du projet. Cette mise à jour vise à rendre le système plus robuste. Les principaux changements sont:

- la mise à jour à la version 2.1.1 de Pandoc;
- l'utilisation de \Hologo{XeLaTeX} comme moteur pour le rendu;
- l'abandon du gabarit pour être remplacé par un préambule;
- l'ajout des fichiers source du présent document.

__Pour passer de la version 1 à la version 2:__

- le fichiers `pdf.sh` doit être remplacé par la nouvelle version;
- le fichiers `preambule.txt` doit être ajouté dans le répertoire racine;
- le début du fichier `document-base.md` doit être modifié;
- le fichier `gabarit.txt` n'est plus requis.

## Qualités recherchées

Les principales qualités recherchées dans une solution pour faciliter la production d'un rapport [^un-rapport] sont:

- une solution fonctionnant sous tous les principaux systèmes d'exploitation:

    - Linux/Unix;
    - Mac OS X;
    - Windows;

- une solution sous licence libre, incluant les polices de caractère;

- une solution facilitant au maximum la saisie du texte;

- une solution produisant un rendu professionnel respectant les normes typographiques de la langue française incluant la césure des mots;

- une solution ayant un apprentissage _relativement_ facile;

- une solution permettant la gestion des versions d'un document.

<!--
\begin{checkhyphens}{}
signal contener événement algèbre césure
\end{checkhyphens}
-->

## Séparation du fond et de la forme

Est-il préférable qu'un outil de rédaction prenne en compte à la fois le fond et la forme ou, au contraire, qu'il sépare ces deux aspects? Cette question shakespearienne nous hante tous. Un article de Wikipédia [^un-separation] prêche pour une séparation de ces deux aspects. Le fond pouvant être rendu ultérieurement sous plusieurs formats --- HTML, PDF, diapositives, etc. Ce débat se décline souvent avec les acronymes WYSIWYG --- _what you see is what you get_ --- et WYSIWYM --- _what you see is what you mean_. [^un-wysiwyg]

Pour ma part, j'ai fait mon nid. Je suis un partisan de la séparation du fond et de la forme. Cela me permet de rédiger plus vite et de me concentrer sur le contenu confiant la mise en forme ultérieure à l'ordinateur. Le champion toute catégorie pour la mise en forme est \LaTeX. [^un-latex] Reste le choix du langage de balisage pour exprimer la structure du document sans m'encombrer, si possible, d'un langage de balisage lourd à apprendre et à utiliser. \LaTeX\ à ce niveau n'est pas l'idéal.

## Et John Gruber créa Markdown...

Markdown [^un-markdown] est un langage de balisage très simple créé en 2004 par John Gruber. Son apprentissage ne prend que quelques minutes. Des éditeurs de texte [^un-editeur] sont offerts pour les ordinateurs, les tablettes et les téléphones intelligents. Markdown est facile à lire comme tel, sans plus de mise en forme. Il permet de rédiger rapidement des courriels, des notes, des listes, des documents plus complexes, par exemple le présent document. Utilisé en intrant, il existe une multitude de logiciels permettant de le transformer en pages HTML, en documents PDF, etc.

Markdown est-il un bon outil pour les auteurs ? Pour certains [^un-ghost], dont moi-même, la réponse est oui. D'autres sont d'avis contraire.

## Pandoc : le couteau suisse de la transformation de texte

Résumons où nous en sommes:

1. Nous voulons éviter les sables mouvants des traitements de texte pour rédiger des rapports selon un format normalisé comme demandé par les universités.

1. Nous voulons séparer le fond et la forme du document pour faciliter la rédaction, le rendu professionnel étant pris en charge par \LaTeX.

1. Nous voulons saisir le texte dans un langage de balisage simple comme Markdown.

1. Nous voulons trouver un outil pour passer du Markdown vers \LaTeX\ automatiquement.

Ce dernier jalon est rendu possible grâce à Pandoc [^un-pandoc], qualifié de couteau suisse de la transformation d'un document d'un format à un autre. Pandoc existe depuis plus de dix ans. Il est utilisé dans une foule de contextes. Il accepte des documents sous une vingtaine de formats différents et peut les transformer vers une quarantaine de formats différents.

## Ce n'est pas pour vous si...

Ce qui va suivre n'est pas pour vous, si:

- vous ne voulez pas apprendre du nouveau;

- vous ne voulez pas installer quelques logiciels sur votre ordinateur;

- vous ne voulez pas lancer une commande dans l'application _Terminal_;

- vous ne voulez pas écrire d'autres rapports dans votre vie;

- vous voulez continuer à passer vos nuits blanches en duel avec votre traitement de texte, chacun choisissant ses combats.

<!-- Notes de bas de page -->

[^un-rapport]: Pour la suite du document, le terme _rapport_ inclut les mémoires et autres textes du même genre.

[^un-separation]: <https://fr.wikipedia.org/wiki/S%C3%A9paration_du_fond_et_de_la_forme>.

[^un-wysiwyg]: <http://romy.tetue.net/wysiwyg-wysiwym-wysiwyc>.

[^un-latex]: Article d'Aurélien Pierre sur les avantages de \LaTeX\ sur les traitements de texte modernes sur le site  <https://aurelienpierre.com/de-lavantage-de-latex-sur-les-traitements-de-texte-modernes/>.

[^un-markdown]: <https://fr.wikipedia.org/wiki/Markdown>.

[^un-editeur]: <https://fr.wikipedia.org/wiki/%C3%89diteur_de_texte>.

[^un-ghost]: <https://blog.ghost.org/markdown/>.

[^un-pandoc]: <http://pandoc.org/index.html>.
