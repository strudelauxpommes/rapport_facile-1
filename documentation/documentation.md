
<!--
    Version du 2020-11-18 - Louis Martin
-->


<!-- Page de titre -->
\begin{titlepage}
    \begin{center}
    { \setstretch{1.2} \large
        \MakeUppercase{
            { \Large Université du Québec à Montréal }
            \vfill
            Outil d'aide à la rédaction de documents
            \vfill
            Documentation d'utilisation
            \vfill
            par \break Louis Martin
            \vfill
            novembre 2020
        }
    }
    \end{center}
\end{titlepage}

<!-- Pagination en chiffre romain au départ -->
\pagenumbering{roman}
\setcounter{page}{2}

<!-- Optionnellement, inclure ci-après les remerciements -->



<!-- Optionnellement, inclure ci-après la dédicace -->
<!-- La dédicace est justifiée à droite -->

# Dédicace {-}

\begin{flushright} {\itshape

Je dédie ce document à tous les étudiantes et étudiants\break
ayant passé des nuits à lutter avec un traitement\break
de texte ingrat et non adapté pour la tâche.

} \end{flushright}


<!-- Optionnellement, inclure ci-après l'avant-propos -->



<!-- Commandes pour la génération de la table des matières et des pages associées -->

\tableofcontents

<!--
\listoffigures

\listoftables

\lstlistoflistings

-->

<!-- Optionnellement, inclure ci-après les abréviations, sigles et acronymes -->

# Liste des abréviations, sigles et acronymes {-}

HTML
  ~ HyperText Markup Language

PDF
  ~ Portable Document Format

WYSIWYG
  ~ What you see is what you get

WYSIWYM
  ~ What you see is what you mean

<!-- Inclure ci-après le résumé -->
<!-- L'espacement entre les lignes est augmenté -->

\onehalfspacing



<!-- Forcer une fin de page, la pagination est remise en chiffre romain et le compteur de page à un, l'espacement entre les lignes est augmenté  -->

\singlespacing
\newpage
\setcounter{page}{1}
\pagenumbering{arabic}

<!-- Inclure ci-après le corps du mémoire dans l'ordre désiré -->

%inclure:textes/introduction.md

%inclure:textes/installation.md

%inclure:textes/utilisation.md

<!-- Le début des annexes est indiqué -->

\appendix

<!-- Inclure ci-après les annexes -->



<!-- Inclure ci-après la bibliographie -->



<!--
    Note : les principales commandes d'espacement sont :
    \singlespacing
    \onehalfspacing
    \doublespacing
-->
