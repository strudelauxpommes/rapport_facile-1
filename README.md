# LISEZ-MOI

## Historique des modifications

### 18 novembre 2020

- Ajustement pour le traitement de la bibliographie avec la nouvelle option de _Pandoc_

- Ajout de deux liens pour faciliter l'installation sous _Linux_.

- Mises à jour mineures pour les nouvelles versions de _LaTeX_, _Pandoc_ et _Python_.

### 5 juillet 2018

- Un exemple additionnel de table est ajouté.

- Le fichier `preambule.txt` est modifié.

### 4 juillet 2018

- Un exemple additionnel de table est ajouté.

- Une question de la FAQ pointe vers des générateurs de tables.

- Corrections mineures

### 30 juin 2018

- L'introduction et la conclusion sont numérotées pour permettre une numérotation correcte des tableaux ou des figures qu'elles pourraient contenir. Le guide de rédaction le permet.

- Les hyperliens pointent maintenant vers le haut des figures et non plus vers le bas facilitant la lecture.

- Le fichier `preambule.txt` est modifié.

### 27 juin 2018

- Ajout d'un exemple de table plus complexe codée en LaTeX --- voir dans le chapitre _Conclusion_ de l'exemple. Le fichier `preambule.txt` est modifié.

### 26 juin 2018

- Ajout d'un exemple de renvoi à un chapitre ou une section. Voir la FAQ pour plus de détail.

### 25 juin 2018

- Le fichier `preambule.txt` est modifié pour forcer l'ouverture à droite des chapitres et pour tout mettre en Times New Roman.

### 22 juin 2018

- Le fichier `preambule.txt` est modifié pour pouvoir utiliser `tabularx` pour les tableaux complexes.

### 20 juin 2018

- Le fichier `preambule.txt` est modifié.
- Permettre l'inclusion d'une section d'un document PDF. Voir la FAQ.
- Permettre de disposer une figure en mode paysage.
- L'exemple est modifié pour illustrer les points précédents.

### 30 mars 2018

- Refactoring
- Un exemple complet a été créé dans le répertoire `exemple`.
- La structure du dépôt GIT a été simplifié.

## Introduction

La tâche de rédiger un rapport ou un mémoire est déjà assez compliquée au niveau du contenu sans que nous soyons obligés de nous battre avec un traitement de texte pour tenter de respecter la mise en page requise.

Ayant entendu des commentaires de plusieurs étudiants étant passés à travers cette épreuve, j'ai voulu voir si une solution plus facile était possible.

## Qualités recherchées

Les principales qualités recherchées dans une solution pour faciliter la production d'un rapport sont :

- une solution fonctionnant sous tous les principaux systèmes d'exploitation :

    - Linux/Unix ;
    - Mac OS X ;
    - Windows ;

- une solution sous licence libre, incluant les polices de caractère ;

- une solution facilitant au maximum la saisie du texte ;

- une solution produisant un rendu professionnel respectant les normes typographiques de la langue française incluant la césure des mots ;

- une solution ayant un apprentissage _relativement_ facile ;

- une solution permettant la gestion des versions d'un document.

## Documentation et exemples

La documentation se trouve dans le répertoire `documentation`.

Un exemple complet se trouve dans le répertoire `exemple`.

Une structure de départ se trouve dans le répertoire `exemple-base`.

----

# Foire aux questions - FAQ

### Comment faire un renvoi à une figure ou à un tableau ?

La balise pour inclure une figure est du type :

    ![Légende de la figure](images/figure_a.png)

La balise pour permettre un renvoi est du type :

    ![Légende de la figure\label{nom_du_label}](images/figure_a.png)

Pour un renvoi à la figure, inclure dans le _markdown_ :

    \ref{nom_du_label}

Pour un renvoi à la page de la figure, inclure dans le _markdown_ :

    \pageref{nom_du_label}

Exemple :

    Ainsi, la figure \ref{nom_du_label} à la page \pageref{nom_du_label} ...

Pour un tableau, ajouter la balise `\label{nom_du_label}` à la fin du titre du tableau. Les renvois sont les mêmes que pour les figures.

### Comment faire un renvoi à un chapitre ou à une section ?

Pandoc crée des labels pour les chapitres et les sections permettant ainsi de générer la table des matières.

Voir le lien <https://pandoc.org/MANUAL.html#headers-and-sections> pour comprendre comment les labels sont générés. En gros :

- les espaces sont remplacées par des tirets
- tous les caractères autres que des lettres ou des chiffres sont retirés
- tous les caractères sont mis en minuscules
- les caractères accentués sont remplacés par les valeurs de la table suivante :

| **Caractère** | **Valeur** |
| :-------- | :------------------ |
| à | uxe0 |
| â | uxe2 |
| æ | uxe6 |
| ç | uxe7 |
| è | uxe8 |
| é | uxe9 |
| ê | uxea |
| ë | uxeb |
| î | uxee |
| ï | uxef |
| ô | uxf4 |
| œ | ux153 |
| ù | uxf9 |
| û | uxfb |
| ü | uxfc |

Pour un renvoi incluant le nom du chapitre ou de la section, inclure dans le _markdown_ :

    \nameref{nom-du-label}

Pour un renvoi à la page, inclure dans le _markdown_ :

    \pageref{nom-du-label}

Exemple :

    Ici nous faisons une référence au chapitre \nameref{conduite-de-lenquuxeate} de la page \pageref{conduite-de-lenquuxeate}.

### Comment faire des références bibliographiques ?

Pour les références bibliographiques, plusieurs styles sont disponibles. Sur le site de <https://www.zotero.org/styles>, il y a plus de 9 000 styles différents.

Pour vous aider voir les liens suivants :

- <https://pandoc.org/MANUAL.html#citations> à la section _Citations_
- <https://pandoc.org/demos.html> à l'item 24

Nous avons ajouté au fichier `pdf.sh` l'option `--citeproc` pour la génération de la bibliographie.

_Pandoc_ ajoutera le contenu de la bibliographie à la toute fin du document.

### Comment inclure une section d'un document PDF ?

Avec la commande `\includepdf`, voir l'annexe 2 de l'exemple. Voici un exemple d'une telle commande:

    \includepdf[pages=1-3,scale=0.70,pagecommand={},frame=true]{images/Rapport_Chaos_2015.pdf}

Documentation de la commande <http://texdoc.net/texmf-dist/doc/latex/pdfpages/pdfpages.pdf>

### Comment inclure une figure en mode paysage ?

Voici un exemple :

    \begin{sidewaysfigure}
        \centering
        \includegraphics[width=\linewidth]{images/image-a.jpg}
        \caption{Exemple d'insertion d'une figure en mode paysage}
        \label{nom_du_label}
    \end{sidewaysfigure}

### Comment coder des tables plus élaborées en LaTeX ?

Le site <http://www.tablesgenerator.com/> offre un générateur de table facilitant le travail.

Un autre site est <http://truben.no/table/>.

Veuillez noter que le package _booktabs_ est déjà chargé dans le préambule permettant ainsi un rendu plus professionnel.
